<?php

namespace Database\Seeders;

use App\Models\Bee;
use App\Models\Flower;
use Illuminate\Database\Seeder;

class FlowerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Flower::factory(36)->create()->each(function ($flower) {
            $bees = Bee::all()->random(2);
            $months = [
                ['month' => rand(1,12),],
                ['month' => rand(1,12),],
                ['month' => rand(1,12),],
            ];
            $flower->bees()->attach($bees);
            $flower->months()->createMany($months);
        });
    }
}
