<?php

namespace Database\Factories;

use App\Models\Bee;
use Illuminate\Database\Eloquent\Factories\Factory;

class BeeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Bee::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->name,
            'specie' => $this->faker->unique()->userName,
        ];
    }
}
