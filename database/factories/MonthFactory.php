<?php

namespace Database\Factories;

use App\Models\Flower;
use App\Models\Month;
use Illuminate\Database\Eloquent\Factories\Factory;

class MonthFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Month::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'month' => $this->faker->numberBetween(1,12),
            'flower_id' => Flower::all()->random()->id
        ];
    }
}
