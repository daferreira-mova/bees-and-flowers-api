<?php

use Faker\Factory;
use Illuminate\Http\UploadedFile;
use Laravel\Lumen\Testing\DatabaseMigrations;

class FlowerTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();

        $this->artisan('db:seed --class=TestSeeder');
    }

    public function testShouldReturnAllFlowers()
    {
        $this->get('/flowers');

        $this->seeStatusCode(200);

        $this->seeJsonStructure([
            'meta' => [
                'status',
                'message',
            ],
            'data' => [
                'links',
            ]
        ]);
    }

    public function testShouldReturnAllFlowersByBeeFilter()
    {
        $this->get('/flowers?bee=1');

        $this->seeStatusCode(200);

        $this->seeJsonStructure([
            'meta' => [
                'status',
                'message',
            ],
            'data' => [
            ]
        ]);
    }

    public function testShouldReturnAllFlowersByMonthsFilter()
    {
        $this->get('/flowers?months=1;2;3');

        $this->seeStatusCode(200);

        $this->seeJsonStructure([
            'meta' => [
                'status',
                'message',
            ],
            'data' => [
            ]
        ]);
    }

    public function testShouldReturnAllFlowersByBeeAndMonthsFilter()
    {
        $this->get('/flowers?bee=1&months=1;2;3');

        $this->seeStatusCode(200);

        $this->seeJsonStructure([
            'meta' => [
                'status',
                'message',
            ],
            'data' => [
            ]
        ]);
    }

    public function testShouldReturnFlower()
    {
        $this->get('/flowers/1');

        $this->seeStatusCode(200);

        $this->seeJsonStructure([
            'meta' => [
                'status',
                'message',
            ],
            'data' => [
            ]
        ]);
    }

    public function testShouldCreateFlowerAndReturnIt()
    {
        $faker = Factory::create();

        $image = UploadedFile::fake()->create('file.png', 5*1000, 'image/png');

        $data = [
            'name' => $faker->name,
            'specie' => $faker->unique()->userName,
            'description' => $faker->text(250),
            'bees' => '1',
            'months' => '5,3'
        ];

        $this->call( 'POST', '/flowers', $data, [], ['image' => $image]);

        $this->seeStatusCode(201);

        $this->seeJsonStructure([
            'meta' => [
                'status',
                'message',
            ],
            'data' => [
            ]
        ]);
    }
}
