<?php

use Laravel\Lumen\Testing\DatabaseMigrations;

class BeeTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();

        $this->artisan('db:seed --class=TestSeeder');
    }

    public function testShouldReturnAllBees()
    {

        $this->get('/bees');

        $this->seeStatusCode(200);

        $this->seeJsonStructure([
            'meta' => [
                'status',
                'message',
                'count'
            ],
            'data' => [
            ]
        ]);
    }

    public function testShouldReturnBee()
    {
        $this->get('/bees/1');

        $this->seeStatusCode(200);

        $this->seeJsonStructure([
            'meta' => [
                'status',
                'message',
            ],
            'data' => [
            ]
        ]);
    }
}
