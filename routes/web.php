<?php

/** @var Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Laravel\Lumen\Routing\Router;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'bees'], function ($router) {
    $router->get('/', [
        'as' => 'index-bees',
        'uses' => 'BeeController@index'
    ]);

    $router->get('/{id:[0-9]+}', [
        'as' => 'show-bees',
        'uses' => 'BeeController@show'
    ]);
});

$router->group(['prefix' => 'flowers'], function ($router) {
    $router->get('/', [
        'as' => 'index-flowers',
        'uses' => 'FlowerController@index'
    ]);

    $router->post('/', [
        'as' => 'create-flowers',
        'uses' => 'FlowerController@create'
    ]);

    $router->get('/{id:[0-9]+}', [
        'as' => 'show-flowers',
        'uses' => 'FlowerController@show'
    ]);
});
