<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @method static find(int $id)
 * @method static factory(int $int)
 */
class Flower extends Model
{

    use HasFactory;

    protected $fillable = [
        'name',
        'specie',
        'description',
        'image_url'
    ];

    protected $with = [
        'bees',
        'months'
    ];

    public function bees(): BelongsToMany
    {
        return $this->belongsToMany(Bee::class, 'bee_flower', 'flower_id', 'bee_id', 'id');
    }

    public function months(): HasMany
    {
        return $this->hasMany(Month::class);
    }
}
