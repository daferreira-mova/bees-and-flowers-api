<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @method static find(int $id)
 * @method static where(string $string, mixed|string $bee)
 */
class Bee extends Model
{
    use HasFactory;

    public function flowers(): BelongsToMany
    {
        return $this->belongsToMany(Flower::class, 'bee_flower', 'bee_id', 'flower_id', 'id');
    }
}
