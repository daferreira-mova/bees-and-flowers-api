<?php

namespace App\Http\Controllers;

use App\Models\Bee;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BeeController extends Controller
{
    public function index(): JsonResponse
    {
        $bees = Bee::all();

        if ($bees->isEmpty()) {
            return response()->json([
                'meta' => [
                    'status' => 'fail',
                    'message' => 'No bees'
                ]
            ], 404);
        }

        return response()->json([
            'meta' => [
                'status' => 'success',
                'message' => 'Bees founded',
                'count' => count($bees)
            ],
            'data' => $bees
        ]);
    }

    public function show(int $id): JsonResponse
    {
        $validator = Validator::make(['id' => $id], [
            'id' => 'required|integer|min:1'
        ], [
            'required' => 'The :attribute is required',
            'integer' => 'The :attribute must be integer',
            'min' => 'The :attribute must be equal or bigger then :min'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'meta' => [
                    'status' => 'fail',
                    'message' => $validator->errors(),
                ]
            ], 403);
        }

        $bee = Bee::find($id);

        if (is_null($bee)) {
            return response()->json([
                'meta' => [
                    'status' => 'fail',
                    'message' => 'No bee'
                ]
            ], 404);
        }

        return response()->json([
            'meta' => [
                'status' => 'success',
                'message' => 'Bee founded',
            ],
            'data' => $bee
        ]);
    }

    public function create(Request $request)
    {

    }

}
