<?php

namespace App\Http\Controllers;

use App\Models\Bee;
use App\Models\Flower;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class FlowerController extends Controller
{

    public function index(Request $request): JsonResponse
    {
        $beeSearch = $request->has('bee') ? $request->query('bee') : null;
        $monthSearch = $request->has('months') ? explode(';', $request->query('months')) : null;

        $flowers = $this->filterMount($beeSearch, $monthSearch);

        if ($flowers->isEmpty()) {
            return response()->json([
                'meta' => [
                    'status' => 'fail',
                    'message' => 'No flowers'
                ]
            ], 404);
        }

        return response()->json([
            'meta' => [
                'status' => 'success',
                'message' => 'Flowers founded',
            ],
            'data' => $flowers
        ]);
    }

    public function show(int $id): JsonResponse
    {
        $validator = Validator::make(['id' => $id], [
            'id' => 'required|integer|min:1'
        ], [
            'required' => 'The :attribute is required',
            'integer' => 'The :attribute must be integer',
            'min' => 'The :attribute must be equal or bigger then :min'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'meta' => [
                    'status' => 'fail',
                    'message' => $validator->errors(),
                ]
            ], 403);
        }

        $flower = Flower::find($id);

        if (is_null($flower)) {
            return response()->json([
                'meta' => [
                    'status' => 'fail',
                    'message' => 'No flower'
                ]
            ], 404);
        }

        return response()->json([
            'meta' => [
                'status' => 'success',
                'message' => 'Flower founded',
            ],
            'data' => $flower
        ]);
    }

    public function create(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'specie' => 'required|string|unique:App\Models\Flower,specie',
            'description' => 'nullable|string',
            'image' => 'required|image',
            'bees' => 'required|string|regex:/^(\d+,)*\d+$/',
            'months' => 'required|string|regex:/^(\d+,)*\d+$/',
        ], [
            'required' => 'The :attribute is required',
            'string' => 'The :attribute must be a string',
            'unique' => 'The :attribute must be unique',
            'image' => 'The :attribute must be a image a type',
            'regex' => 'The :attribute must match with pattern (number,number2,number3)'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'meta' => [
                    'status' => 'fail',
                    'message' => $validator->errors(),
                ]
            ], 403);
        }

        $image = $request->file('image');
        if (is_null($image)) {
            return response()->json([
                'meta' => [
                    'status' => 'fail',
                    'message' => 'Image is null',
                ]
            ], 403);
        }
        $mimeType = explode('/', $image->getMimeType());
        $extension = end($mimeType);
        $image = $image->move('images', Str::slug($request->get('specie'), '_')  . "_" . time() . ".$extension");

        $data = $request->except('image');
        $data['image_url'] = $image->getPathname();

        $flower = new Flower();
        $flower->fill($data);

        $bees = explode(',', $data['bees']);
        $months = explode(',', $data['months']);

        foreach ($months as $month) {
            if ($month > 12) {
                return response()->json([
                    'meta' => [
                        'status' => 'fail',
                        'message' => 'The month number must be between 1 and 12',
                    ]
                ], 403);
            }

            $monthsFormatted[] = [
                'month' => $month
            ];
        }

        foreach ($bees as $bee) {
            if (Bee::where('id', $bee)->exists() === false) {
                return response()->json([
                    'meta' => [
                        'status' => 'fail',
                        'message' => 'The bee id must be exists',
                    ]
                ], 403);
            }
        }

        if ($flower->save() === false) {
            return response()->json([
                'meta' => [
                    'status' => 'fail',
                    'message' => 'Error persisting in the database'
                ],
            ]);
        }

        $flower->bees()->attach($bees);
        $flower->months()->createMany($monthsFormatted);

        $flower = Flower::find($flower->id);

        return response()->json([
            'meta' => [
                'status' => 'success',
                'message' => 'Flower created'
            ],
            'data' => $flower
        ], 201);
    }

    private function filterMount($beeSearch, $monthSearch): LengthAwarePaginator
    {
        $query = Flower::query();

        if (!is_null($beeSearch)) {
            $query = $query->whereHas('bees', function ($query) use ($beeSearch) {
                $query->where('bees.id', '=', $beeSearch);
            });
        }

        if (!is_null($monthSearch)) {
            $query = $query->whereHas('months', function ($query) use ($monthSearch) {
                return $query->whereIn('months.month', $monthSearch);
            });
        }

        return $query->orderBy('name')->paginate(12);
    }

}
